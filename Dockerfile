FROM openjdk:11
ARG JAR_FILE=target/*.jar
EXPOSE 9090
ADD $JAR_FILE my-app.jar
ENTRYPOINT ["java","-jar","/my-app.jar"]
